﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Moq;

using NerdDinner.AppService.Consumers;
using NerdDinner.AppService.Domain;
using NerdDinner.Messages;
using NerdDinner.Messages.Commands;

using NUnit.Framework;

namespace NerdDinner.Test.AppService
{
    [TestFixture]
    public class CreateDinnerFixture : BaseDinnerConsumerFixture
    {

        #region Fields

        #endregion

        [Test]
        public void NewDinnerWhenConsumedShouldSaveToRepository()
        {

            // Arrange
            _MockRepo.Setup(r => r.Save(It.IsAny<Dinner>(), It.IsAny<Guid>(), null))
                .Verifiable("Did not save the Dinner domain object");

            _MockContext.Setup(c => c.Respond(It.Is<BusResponse>(r => r.Success), NullAction))
                .Verifiable("Did not transmit a success message");

            var createMsg = new CreateDinner();

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) {_ConsumeContext = _MockContext.Object};
            sut.Consume(createMsg);

            // Assert
            _MockRepo.VerifyAll();

        }

        [Test]
        public void NewDinnerWithErrorDuringSaveShouldReturnErrorMessage()
        {
            
            // Arrange
            var testError = new Exception("UNIT TEST ERROR");
            _MockRepo.Setup(r => r.Save(It.IsAny<Dinner>(), It.IsAny<Guid>(), null))
                .Throws(testError);

            _MockContext.Setup(c => c.Respond(It.Is<BusResponse>(r => !r.Success && r.Exception.Equals(testError)), NullAction))
                .Verifiable("Did not return a failure status and the exception thrown");

            var createMsg = new CreateDinner();

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object);
            sut._ConsumeContext = _MockContext.Object;
            sut.Consume(createMsg);

            // Assert
            _MockContext.VerifyAll();

        }

        [Test]
        public void NewDinnerWithErrorDuringCreateShouldReturnErrorMessage()
        {
            
            // Arrange
            var testError = new Exception("UNIT TEST ERROR");
            var createMsg = new Mock<CreateDinner>();
            createMsg.SetupGet(c => c.Country).Throws(testError);
            

            _MockContext.Setup(c => c.Respond(It.Is<BusResponse>(r => !r.Success && r.Exception.Equals(testError)), NullAction))
                .Verifiable("Did not return a failure status and the exception from the create dinner method");

            // Act
            var sut = new DinnerConsumer(_MockRepo.Object) {_ConsumeContext = _MockContext.Object};
            sut.Consume(createMsg.Object);

            // Assert
            _MockContext.VerifyAll();

        }

    }
}
