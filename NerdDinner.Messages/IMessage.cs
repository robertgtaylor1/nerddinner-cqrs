﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MassTransit;

namespace NerdDinner.Messages
{

    public interface IMessage : CorrelatedBy<Guid>
    {
        DateTime ActingDateTimeUtc { get; set; }
        string ActingLogonId { get; set; }
        Guid AggregateId { get; set; }
        int Version { get; set; }
    }

    public interface ICommand : IMessage
    {
    }

    public interface IEvent : IMessage
    {
    }
}
