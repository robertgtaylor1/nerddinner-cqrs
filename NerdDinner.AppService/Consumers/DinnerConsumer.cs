﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CommonDomain.Persistence;

using MassTransit;
using MassTransit.Context;

using NerdDinner.AppService.Domain;
using NerdDinner.Messages;
using NerdDinner.Messages.Commands;

namespace NerdDinner.AppService.Consumers
{

    public class DinnerConsumer : Consumes<CreateDinner>.All, Consumes<RescheduleDinner>.All
    {

        #region Fields

        private IRepository _Repo;
        internal IConsumeContext _ConsumeContext;

        #endregion
        
        public DinnerConsumer(IRepository repo)
        {
            _Repo = repo;
        }

        public void Consume(CreateDinner msg)
        {
            Dinner d;
            try
            {
                d = Dinner.New(msg.AggregateId, msg.Title, msg.Description, msg.EventDate, msg.HostedByLogon, msg.Address, msg.Country, msg.ContactPhone, msg.Latitude, msg.Longitude, msg.ActingDateTimeUtc, msg.ActingLogonId);
            }
            catch (Exception e)
            {
                MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Exception = e, Message ="Error while creating dinner", Success = false}, x => { });
                return;
            }

            try
            {
                _Repo.Save(d, Guid.NewGuid(), null);
            } catch (Exception e)
            {
                MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Exception = e, Message = "Error while saving dinner", Success = false}, x => { });
                return;
            }
            
            MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Message = "Dinner created successfully", Success = true}, x => { });

        }

        public void Consume(RescheduleDinner msg)
        {
            Dinner d = null;
            try
            {
                d = _Repo.GetById<Dinner>(msg.AggregateId);
            }
            catch (Exception e)
            {
                MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Exception = e, Message = "Unable to fetch dinner from repository", Success = false});
                return;
            }

            if (d == null)
            {
                MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Message = "Unable to locate dinner in repository", Success = false});
                return;
            }

            try
            {
                d.RescheduleDinner(msg.NewEventTime, msg.ActingDateTimeUtc, msg.ActingLogonId);
            }
            catch (Exception e)
            {
                MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Exception = e, Message = "Unable to reschedule dinner", Success = false});
                return;
            }

            try
            {
                _Repo.Save(d, Guid.NewGuid(), null);
            }
            catch (Exception e)
            {
                MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Exception = e, Message = "Unable to save dinner to repository", Success = false});
                return;
            }

            MyMessageContext().Respond(new BusResponse() {CorrelationId = msg.CorrelationId, Message = "Dinner rescheduled successfully", Success = true});

        }

        public IConsumeContext MyMessageContext()
        {
            return _ConsumeContext ?? this.Context();
        }

    }
}
