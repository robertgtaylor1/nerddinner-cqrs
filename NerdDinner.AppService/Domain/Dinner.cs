﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CommonDomain.Core;

using NerdDinner.Messages.Events;

namespace NerdDinner.AppService.Domain
{
    
    public class Dinner : AggregateBase
    {
        internal DateTime EventDate;

        private Dinner(Guid id)
        {
            this.Id = id;
        }

        [Obsolete("Provided for mocking purposes")]
        public Dinner() {}

        protected Dinner(Guid id, string title, string description, DateTime eventDate, string hostedByLogon, string address, string country, string contactPhone, double lat, double lon, DateTime actDate, string actUser)
        {

            RaiseEvent(new DinnerCreated()
                           {
                               AggregateId = id,
                               ActingDateTimeUtc = actDate,
                               ActingLogonId = actUser,
                               Address = address,
                               ContactPhone = contactPhone,
                               Country = country,
                               Description = description,
                               EventDate = eventDate,
                               HostedBy = hostedByLogon,
                               Latitude = lat,
                               Longitude = lon,
                               Title = title,
                               CorrelationId = Guid.NewGuid(),
                               Version = 1
                           });

        }

        public static Dinner New(Guid id, string title, string description, DateTime eventDate, string hostedByLogon, string address, string country, string contactPhone, double lat, double lon, DateTime actDate, string actUser)
        {
            return new Dinner(id, title, description, eventDate, hostedByLogon, address,country,contactPhone, lat, lon, actDate, actUser);
        }

        public virtual void RescheduleDinner(DateTime newTime, DateTime actDate, string actUser)
        {
            
            // Check that the new date is in the past
            if (newTime < DateTime.Now) throw new ApplicationException("Cannot reschedule a dinner into the past");

            RaiseEvent(new DinnerRescheduled()
                           {
                               NewEventTime = newTime,
                               ActingDateTimeUtc = actDate,
                               ActingLogonId = actUser,
                               AggregateId = Id,
                               CorrelationId = Guid.NewGuid(),
                               Version = Version
                           });

        }

        #region Private Event Apply

        private void Apply(DinnerCreated e)
        {
            this.Id = e.AggregateId;
            this.EventDate = e.EventDate;
        }

        private void Apply(DinnerRescheduled e)
        {
            this.EventDate = e.NewEventTime;
        }

        #endregion

    }

}
