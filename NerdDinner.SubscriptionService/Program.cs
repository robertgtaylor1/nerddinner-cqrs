﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MassTransit;
using MassTransit.Saga;
using MassTransit.Services.Subscriptions.Server;

using Topshelf;

namespace NerdDinner.SubscriptionService
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(host =>
                                {
                                    host.Service<AppService>(s =>
                                                                 {
                                                                     s.SetServiceName("NerdDinner.SubscriptionService");
                                                                     s.ConstructUsing(c => new AppService());
                                                                     s.WhenStarted(c => c.RunService());
                                                                     s.WhenStopped(c => c.Dispose());
                                                                 });
                                    host.RunAsLocalService();
                                    host.SetDescription("NerdDinner Subscription Service");
                                    host.SetDisplayName("NerdDinner SubService");
                                    host.SetServiceName("NerdDinner.Subservice");

                                });
        }
    } 

    public class AppService :IDisposable {

        private static IServiceBus _SubscriptionBus;
        private static string SubscriptionUri = "msmq://localhost/NerdDinner.SubscriptionService";
        private static MassTransit.Services.Subscriptions.Server.SubscriptionService _SubscriptionService;

        public void RunService()
        {

            // MassTransit - Subscription Service
            try
            {
                _SubscriptionBus = ServiceBusFactory.New(sbc =>
                {
                    sbc.UseMsmq();
                    sbc.VerifyMsmqConfiguration();
                    sbc.ReceiveFrom(SubscriptionUri);
                });
                var sagas = new InMemorySagaRepository<SubscriptionSaga>();
                var clientSagas = new InMemorySagaRepository<SubscriptionClientSaga>();
                _SubscriptionService = new MassTransit.Services.Subscriptions.Server.SubscriptionService(_SubscriptionBus, sagas, clientSagas);
                _SubscriptionService.Start();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error configuring SubscriptionService: {0}", e);
                Console.ReadLine();
            }

        }

        public void Dispose()
        {
            _SubscriptionService.Dispose();
            _SubscriptionBus.Dispose();
        }
    }
}
