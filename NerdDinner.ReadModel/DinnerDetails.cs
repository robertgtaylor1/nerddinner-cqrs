﻿using System;
using System.Linq;

using MongoDB.Bson.Serialization.Attributes;

namespace NerdDinner.ReadModel
{
    public class DinnerDetails : IStoredInMongo
    {

        [BsonElement("dt")]
        public DateTime EventDate { get; set; }

        [BsonElement("addr")]
        public string Address { get; set; }

        [BsonElement("country")]
        public string Country { get; set; }

        [BsonElement("lat")]
        public double Latitude { get; set; }

        [BsonElement("lon")]
        public double Longitude { get; set; }

        [BsonElement("desc")]
        public string Description { get; set; }

        [BsonElement("host")]
        public string HostedBy { get; set; }

        [BsonElement("phone")]
        public string HostPhone { get; set; }

        [BsonElement("rsvp")]
        public string[] RSVPs { get; set; }

        [BsonId]
        public string Id { get; set; }

        [BsonIgnore]
        public string CollectionName { get { return "dinnerdetails"; } }

        public void AddRsvp(string newRsvp)
        {
            var toAdd = RSVPs.ToList();
            toAdd.Add(newRsvp);
            RSVPs = toAdd.ToArray();
        }

    }
}