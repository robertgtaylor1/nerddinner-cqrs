﻿using MassTransit;

using NerdDinner.Messages.Events;
using NerdDinner.ReadModel;

namespace NerdDinner.Denormalizer.Consumers
{
    public class DinnerDetailConsumer : Consumes<DinnerCreated>.All, Consumes<DinnerRescheduled>.All
    {

        #region Fields

        private MongoUpdater _Updater;

        #endregion
        
        public DinnerDetailConsumer(MongoUpdater updater)
        {
            _Updater = updater;
        }


        public void Consume(DinnerCreated msg)
        {

            var d = new DinnerDetails()
                        {
                            Address = msg.Address,
                            Country = msg.Country,
                            Description = msg.Description,
                            EventDate = msg.EventDate,
                            HostedBy = msg.HostedBy,
                            HostPhone = msg.ContactPhone,
                            Id = msg.AggregateId.ToString(),
                            Latitude = msg.Latitude,
                            Longitude = msg.Longitude,
                            RSVPs = new string[] {msg.HostedBy}
                        };

            _Updater.Add(d);

        }

        public void Consume(DinnerRescheduled msg)
        {

            var d = _Updater.GetById<DinnerDetails>(msg.AggregateId);
            d.EventDate = msg.NewEventTime;
            _Updater.Update(d);

        }
    }
}