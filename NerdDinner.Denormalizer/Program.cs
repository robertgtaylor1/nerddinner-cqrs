﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

using Topshelf;

namespace NerdDinner.Denormalizer
{
    class Program
    {
        static void Main(string[] args)
        {

            HostFactory.Run(host =>
                                {
                                    host.Service<AppService>(s =>
                                                                 {
                                                                     s.SetServiceName("NerdDinner.Denormalizer");
                                                                     s.ConstructUsing(c => new AppService());
                                                                     s.WhenStarted(c => c.Start());
                                                                     s.WhenStopped(c => c.Dispose());
                                                                 });
                                    host.RunAsLocalService();
                                    host.SetDescription("NerdDinner denormalizer for READ access");
                                    host.SetDisplayName("NerdDinner Denormalizer");
                                    host.SetServiceName("NerdDinner.Denormalizer");
                                });

        }
    }
}
