# NerdDinner-CQRS

A hack of the NerdDinner sample code to be architected in a more CQRS style.  For those 'in the know', we freely admin that the CQRS architecture is a bit
of overkill for the simple domain of Dinners and Users.  However, in an effort to bridge the considerable learning curve of CQRS architecture concepts,
tools, and better practices (as I admittedly have short-cut some practices in this effort) - the NerdDinner project was selected as a common starting point
for learners.

## Prerequisites

This code was written to run with a local install of Sql Server Express and MongoDb.  Check the connection
strings in the web project for details on database names and authentication.  You will find the authentication to
be **HIGHLY** secure.

You will need to create the local *NerdDinner-CQRS* database in you Sql Server instance, but otherwise
the projects will generate the necessary database schema in Sql Server and Mongo on the fly as they are utilized.

The application uses MSMQ for communication between the three nodes.  You will need to have MSMQ installed prior to
running the project.  The servicebus in this solution (MassTransit) will construct the necessary queues for you.

This solution needs to be run with Administrator privileges, due to all of the objects being constructed.

NuGet package sources are NOT included in this repository.  Run a nuget restore on the solution.  Details coming soon on this operation if
you need them.

##  Updates

I will be submitting a few more updates to make this a more complete representation of a CQRS sample.  Additionally,
at some point I will be fixing the original unit tests so they work in this new architecture and adding some more to demonstrate how easy
it is to test the full architecture of reads and writes.

Pull requests are encouraged and appreciated