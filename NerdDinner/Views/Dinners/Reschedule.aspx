<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<NerdDinner.ReadModel.DinnerListItem>"
    MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Title" ContentPlaceHolderID="TitleContent" runat="server">
    Edit:
    <%:Model.Title %>
</asp:Content>
<asp:Content ID="Edit" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Reschedule Dinner</h2>
    <% Html.EnableClientValidation(); %>
    <%: Html.ValidationSummary("Please correct the errors and try again.") %>
    <% using (Html.BeginForm())
       { %>
    <fieldset>
        <div id="dinnerDiv">

        <p>
            <strong>Current Event Date:</strong>
            <abbr><%= Model.EventDate %></abbr>
        </p>

        <p>
            <label for="EventDate">New Event Date:</label>
             <%: Html.TextBox("newTime", String.Format("{0:yyyy-MM-dd HH:mm}", Model.EventDate)) %>
         <script type="text/javascript">
             $(function () {
                 $('#EventDate').datetime({
                     userLang: 'en',
                     americanMode: true
                 });
             });
            </script>
        </p>

        <%= Html.Hidden("DinnerId", Model.Id) %>

            <p>
                <input type="submit" value="Save" />
            </p>
        </div>
    </fieldset>
    <% } %>
</asp:Content>
